<?php
    require_once 'connection.php';
    
    if (!isset($_GET['department']))
    {
        message('Afdeling ikke specificeret.', 'error');
        redirect('/index');
    }
    
    $department = get_department($db, $_GET['department']);
    
    if ($department === null)
    {
        message("Ugyldigt afdelingsnummer: {$_GET['department']}.", 'error');
        redirect('/index');
    }
    
    $_SESSION['department'] = array('id' => $department['id'],
        'name' => $department['department_name']);
    
    if (isset($_SESSION['case']))
    {
        unset($_SESSION['case']);
    }

    require_once 'header.php';

    function draw_cases(array $cases)
    {
        foreach ($cases as $case): ?>
        <li><a href="/case/<?php echo $case['id']; ?>">
        <h2>#<?php echo $case['id'], ': ', str_escape($case['case_name']); ?></h2>
        <p>Oprettet <?php echo $case['created']; ?></p>
        <p>Udløber <?php echo $case['expires']; ?></p>
        </a></li>
<?php endforeach;
    }

    $res = get_cases($db, $department['id']);
?>
    <ul data-role="listview" data-inset="true" data-filter="true">
        <li data-role="list-divider">Aktive Sager</li>
<?php
    if (empty($res)):
?>
        <li>Ingen aktive sager.</li>
<?php
    else:
        draw_cases($res);
    endif;
    
    $res = get_cases($db, $department['id'], false);
?>
        <li data-role="list-divider" data-theme="a">Lukkede Sager</li>
<?php
    if (empty($res)):
?>
        <li>Ingen lukkede sager.</li>
<?php
    else:
        draw_cases($res);
    endif;
?>
    </ul>
<?php require_once 'footer.php';
