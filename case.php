<?php
    require_once 'connection.php';
    
    if (!isset($_GET['case']))
    {
        $error = 'Sagsnummer ikke specificeret.';
    }
    else
    {
        $case = get_case($db, $_GET['case']);
        if (!$case)
        {
            $error = "Ugyldigt sagsnummer: {$_GET['case']}.";
        }
    }

    if (isset($error))
    {
        message($error, 'error');
        if (isset($_SESSION['department']) && is_array($_SESSION['department']))
        {
            redirect("/cases/{$_SESSION['department']['id']}");
        }
        redirect('/index');
    }
    
    $_SESSION['case'] = array('id' => $case['id'],
        'name' => $case['case_name'],
        'active' => $case['active']);

    $_SESSION['time'] = time();

    require_once 'header.php';
?>
    <ul data-role="listview" data-inset="true" data-filter="false">
        <li data-role="list-divider">#<?php echo $case['id'], ': ',
            str_escape($case['case_name']); ?></li>
        <li><p style="width:80%"><strong><?php echo str_escape($case['author']); ?></strong></p>
        <p style="width:80%">Oprettet <?php echo $case['created']; ?></p>
        <p style="width:80%">Udløber <?php echo $case['expires']; ?></p>
		<div class="fb-share-button ui-li-aside" data-type="button" data-href="<?php
				echo "http://www.{$_SERVER['SERVER_NAME']}/case/{$case['id']}"; ?>"></div>
        <?php echo nl2br($case['case_desc']); ?>
        </li>
<?php
    $votes = get_votes($db, $case['id']);
    if (!empty($votes)):
?>
        <li data-role="list-divider">Afstemning</li>
        <li><?php echo $votes[0]['vote_topic']; ?>
<?php
    $vote_id = $votes[0]['vote_id'];
    $votes_cast = max(1, $votes[0]['votes_cast']);
    $has_voted = false;
    
    if (isset($_COOKIE['has_voted'][$vote_id])
        && $_COOKIE['has_voted'][$vote_id])
    {
        $has_voted = true;
    }
    else if (isset($_SESSION['has_voted'][$vote_id])
        && $_SESSION['has_voted'][$vote_id])
    {
        $has_voted = true;
    }
    
    $allow_voting = $case['active'] && !$has_voted;

    if ($allow_voting):
?>
        <form action="/handlevote" method="post" data-ajax="false">
            <input type="hidden" name="case_id"
                value="<?php echo $case['id']; ?>"/>
            <input type="hidden" name="vote_id"
                value="<?php echo $vote_id; ?>"/>
            <fieldset data-role="controlgroup">
<?php
        foreach ($votes as $vote):
            $name = "vote" . ($vote['vote_type'] === 'check' ? '[]' : '');
            $id = "vote-{$vote['option_id']}";
?>
                <input name="<?php echo $name; ?>" id="<?php echo $id; ?>"
                    value="<?php echo $vote['option_id']; ?>" data-mini="true"
                    type="<?php echo $vote['vote_type'] === 'radio'
                            ? 'radio' : 'checkbox'; ?>"/>
                <label for="<?php echo $id; ?>"><?php
                    echo $vote['vote_option']; ?></label>
<?php
        endforeach;
?>
            </fieldset>
            <input type="submit" value="Stem"/>
        </form>
<?php
    else:
?>
            <table class="voteresults ui-body-g ui-shadow table-stripe ui-responsive"
                data-mode="columntoggle" data-column-btn-text="Kolonner..."
                data-role="table">
                <thead>
                    <tr class="ui-bar-d">
                        <th>Valgmulighed</th>
                        <th>Stemmer</th>
                        <th data-priority="1" colspan="2">Fordeling</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td class="voteresults-footer"
                            colspan="4">Stemmer afgivet: <?php
                                echo $votes_cast; ?></td>
                    </tr>
                </tfoot>
                <tbody>
<?php
        foreach ($votes as $vote):
            $pct = $vote['vote_count'] / $votes_cast * 100;
?>
                    <tr>
                        <td class="voteresults-column-text"><?php
                            echo $vote['vote_option']; ?></td>
                        <td class="voteresults-column-number"><?php
                            echo $vote['vote_count']; ?></td>
                        <td class="voteresults-column-number"><?php
                            echo round($pct, 0); ?>%</td>
                        <td class="voteresults-column-bar"><div
                            class="voteresults-bar" style="width:<?php
                            echo $pct; ?>%">&nbsp;</div></td>
                    </tr>
<?php
        endforeach;
?>
                </tbody>
            </table>
<?php
    endif;
?>
        </li>
<?php
    endif;
?>
        <li data-role="list-divider">Kommentarer</li>
<?php
    $comments = get_comments($db, $case['id']);

    if (empty($comments)):
?>
        <li>Ingen kommentarer.</li>
<?php
    else:
        foreach ($comments as $comment):
?>
        <li>
        <p><strong><?php echo $comment['author'] !== ''
            ? str_escape($comment['author'])
            : '<em>Intet navn</em>'; ?></strong></p>
        <p>Oprettet <?php echo $comment['created']; ?></p>
        <?php echo str_escape($comment['content']); ?>
        </li>
<?php
        endforeach;
    endif;
?>
    </ul>
<?php require_once 'footer.php';
