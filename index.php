<?php
    require_once 'connection.php';
    
    unset($_SESSION['department']);
    unset($_SESSION['case']);
    require_once 'header.php';
    
    $res = get_departments($db);
?>
    <ul data-role="listview" data-inset="true" data-filter="true">
        <li data-role="list-divider">Afdelinger</li>
<?php
    if (empty($res)):
?>
        <li>Ingen afdelinger fundet.</li>
<?php
    else:
        foreach ($res as $row):
?>
        <li><a href="/cases/<?php echo $row['id']; ?>">
        <h2>Afdeling <?php echo $row['id'], ': ',
            str_escape($row['department_name']); ?></h2>
        </a></li>
<?php
        endforeach;
    endif;
?>
    </ul>
<?php require_once 'footer.php';
