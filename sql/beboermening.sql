-- phpMyAdmin SQL Dump
-- version 4.0.5
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306

-- Generation Time: Oct 08, 2013 at 03:24 PM
-- Server version: 5.5.33
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `beboermening`
--



-- --------------------------------------------------------

--
-- Table structure for table `cases`
--

DROP TABLE IF EXISTS `cases`;
CREATE TABLE IF NOT EXISTS `cases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `expires` datetime NOT NULL,
  `author` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `case_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `case_desc` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cases`
--

INSERT INTO `cases` (`id`, `department_id`, `created`, `expires`, `author`, `case_name`, `case_desc`) VALUES
(1, 419, '2013-06-13 15:10:48', '2013-08-17 23:59:59', 'Søren Jensen', 'Forfaldent cykelskur', 'Cykelskuret trænger til en grundig istandsættelse -- rådne brædder og strittende søm -- men det er uklart i hvor høj grad skuret rent faktisk benyttes og om en renovation er en fornuftig investering, eller om det måske bare skal rives ned. Hvad mener I?'),
(2, 419, '2013-08-18 11:18:09', '2013-12-25 23:59:59', 'Søren Jensen', 'Maling af cykelskur', 'Det er blevet besluttet at cykelskuret skal renoveres. I den forbindelse er det blevet foreslået at skuret også får en ny farve. Her er en mulighed for at stemme om farvevalget.'),
(3, 419, '2013-07-27 14:08:04', '2013-11-30 23:59:59', 'Janni', 'Ønske: gyngestativ', 'Vi vil meget gerne have et gyngestativ (med mindst to gynger) på legepladsen.');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `case_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `author` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id_idx` (`case_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `case_id`, `created`, `author`, `content`) VALUES
(1, 1, '2013-06-13 15:49:12', 'Søren Jensen', 'Jeg bruger ikke selv skuret men er umiddelbart for en renovation.'),
(2, 1, '2013-06-14 07:26:02', '', 'Jeg vil meget gerne have skuret renoveret! :)'),
(3, 1, '2013-06-14 16:14:17', 'Louise', 'Jeg bruger det kun i sommerhalvåret så for mig er det ikke kritisk, men jeg synes vi bør have et og vores er godt nok grimt.'),
(4, 1, '2013-06-20 15:37:12', 'jens', 'okay'),
(5, 1, '2013-08-13 17:40:10', 'LN', 'Jeg så hellere at parkeringspladsen blev udvidet.'),
(6, 3, '2013-08-02 10:01:12', 'jens', 'ja det lyder godt'),
(7, 3, '2013-08-03 19:00:52', 'Louise', 'Fint; men yderligere detaljer omkring valg af gyngestativ bør behandles uafhængigt af valget om opførelse.'),
(8, 3, '2013-08-05 09:37:42', 'Søren Jensen', 'Jeg har kigget lidt rundt og konkluderet at vi godt kan få plads i budgettet.');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL,
  `department_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department_name`) VALUES
(1, 'Afdeling 1'),
(61, 'Choko-Laden'),
(419, 'Fiktiv');

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

DROP TABLE IF EXISTS `votes`;
CREATE TABLE IF NOT EXISTS `votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `case_id` int(11) NOT NULL,
  `votes_cast` int(11) NOT NULL DEFAULT '0',
  `vote_type` enum('radio','check') COLLATE utf8_unicode_ci NOT NULL,
  `vote_topic` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `case_id` (`case_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `votes`
--

INSERT INTO `votes` (`id`, `case_id`, `votes_cast`, `vote_type`, `vote_topic`) VALUES
(1, 1, 4, 'radio', 'Renovér cykelskur'),
(2, 2, 10, 'check', 'Vælg farve'),
(3, 3, 13, 'radio', 'Byg gyngestativ?');

-- --------------------------------------------------------

--
-- Table structure for table `vote_options`
--

DROP TABLE IF EXISTS `vote_options`;
CREATE TABLE IF NOT EXISTS `vote_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vote_id` int(11) NOT NULL,
  `vote_count` int(11) NOT NULL DEFAULT '0',
  `vote_option` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vote_id_idx` (`vote_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vote_options`
--

INSERT INTO `vote_options` (`id`, `vote_id`, `vote_count`, `vote_option`) VALUES
(1, 1, 3, 'For'),
(2, 1, 1, 'Imod'),
(3, 2, 10, 'Rød'),
(4, 2, 7, 'Blå'),
(5, 2, 5, 'Sort'),
(6, 2, 2, 'Hvid'),
(7, 3, 10, 'Ja'),
(8, 3, 3, 'Nej');
COMMIT;
