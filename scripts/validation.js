function text(value, options) {
    if ((options.min_length && value.length < options.min_length)
        || (options.max_length && value.length > options.max_length)) {
            return false;
        }
        return true;
}

function date(value, options) {
    return !!value.match(options.pattern);
}

function require_if(value, options) {
    return options.dependency.fn(options.dependency.target);
}

function is_selected(target) {
    return document.getElementById(target).checked;
}

function is_not_selected(target) {
    return !document.getElementById(target).checked;
}

var validator = {
    validate: function(event) {
        window.validator.rules.forEach(function(rule) {
            var input = $("#" + rule.name),
            error = input.closest("li").find(".input-error");
            var result = false;
            rule.fn.forEach(function(validator) {
                result |= validator(input.val(), rule.options);
            });
            if (!result) {
                error.addClass("input-error-visible");
                event.preventDefault();
            } else {
                error.removeClass("input-error-visible");
            }
        });
    }
};
