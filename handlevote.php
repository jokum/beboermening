<?php
    require_once 'connection.php';
    
    $location = '/index';
    
    if (!isset($_POST) || !isset($_POST['vote']))
    {
        $err = 'Ingen stemmedata.';
    }
    else if (!isset($_POST['case_id']))
    {
        $err = 'Sagsnummer ikke specificeret.';
    }
    else
    {
        $vote = $_POST['vote'];
        $vote_id = $_POST['vote_id'];
        $case = get_case($db, $_POST['case_id']);
        
        if ($case === null)
        {
            $err = "Ugyldigt sagsnummer: {$_POST['case_id']}.";
        }
        else if (!$case['active'])
        {
            $err = 'Sag udløbet.';
        }
        else if (isset($_COOKIE['has_voted'])
            && isset($_COOKIE['has_voted'][$vote_id])
            && $_COOKIE['has_voted'][$vote_id])
        {
            $err = 'Du har allerede stemt.';
        }
        else if (!isset($_SESSION['time']) || time() - $_SESSION['time'] < 5)
        {
            $err = 'Vent venligst fem sekunder med at trykke stem. Dette er for at forhindre spam.';
        }

        unset($_SESSION['time']);
    }
        
    if (isset($err))
    {
        message($err, 'error');
        if (isset($case))
        {
            $location = "/case/{$case['id']}";
        }
        else if (isset($_SESSION['department']) && is_array($_SESSION['department']))
        {
            $location = "/cases/{$_SESSION['department']['id']}";
        }
        else
        {
            $location = '/index';
        }
        redirect($location);
    }
    
    if (is_array($vote))
    {
        $placeholders = str_repeat('?,', count($vote) - 1) . '?';
        $args = $vote;
    }
    else
    {
        $placeholders = '?';
        $args = array($vote);
    }
    
    try {
        $db->beginTransaction();
        $sql = "UPDATE vote_options SET vote_count = vote_count + 1
            WHERE id IN ({$placeholders})";
        $stmt = $db->prepare($sql);
        $stmt->execute($args);
        $row_count1 = $stmt->rowCount();
        
        $sql = 'UPDATE votes SET votes_cast = votes_cast + 1
            WHERE id = ?';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($vote_id));
        $row_count2 = $stmt->rowCount();
        
        if ($row_count1 && $row_count2)
        {
            $db->commit();
            setcookie("has_voted[{$vote_id}]", 1,
                strtotime('+90 days'));
            $_SESSION['has_voted'][$vote_id] = true;
            message('Stemme afgivet.');
        }
        else
        {
            throw new PDOException();
        }
    } catch (PDOException $e) {
        $db->rollBack();
        message('Fejl under stemmeafgivelse.', 'error');
    }
    
    redirect("/case/{$case['id']}");
