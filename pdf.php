<?php
require_once('connection.php');

if (!isset($_GET['case']))
{
    $msg = 'Sagsnummer ikke specificeret.';
}
else
{
    $case = get_case($db, $_GET['case']);
    if ($case === null)
    {
        $msg = "Ugyldigt sagsnummer: {$_GET['case']}.";
    }
}
if (!empty($msg))
{
    message($msg, 'error');
    if (isset($_SESSION['department']) && is_array($_SESSION['department']))
    {
        redirect("/cases/{$_SESSION['department']['id']}");
    }
    redirect('/index');
}

define ('K_TCPDF_EXTERNAL_CONFIG', true);

require_once('tcpdf_config.php');
require_once('tcpdf/tcpdf.php');

$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Opslag');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setFontSubsetting(false);

$pdf->AddPage();

$QR_style = array(
	'border' => false,
    'position' => 'C',
);

$case_name = $case['case_name'];
$server_name = $_SERVER['SERVER_NAME'];
$case_url = "http://www.{$server_name}/case/{$case['id']}";

$pdf->SetFont('helvetica', 'BU', 24);
$pdf->Write(0, $case_name, '', 0, 'C', true, 0, false, false, 0);

$pdf->write2DBarcode($case_url, 'QRCODE,H', 0, 80, 70, 70, $QR_style, 'N');

$pdf->SetY(180);
$pdf->SetFont('helvetica', '', 16);
$pdf->Write(0, $case_url, $case_url, 0, 'C', true, 0, false, false, 0);

$pdf->Output('Opslag.pdf', 'I');