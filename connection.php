<?php
    ini_set('display_errors',1);
    ini_set('display_startup_errors',1);
    error_reporting(-1);

    session_start();

    $port = 3306;
    $dbname = 'beboermening';
    
    // Must define
    // - $host
    // - $user
    // - $password
    require_once 'db.creds.php';
    
    $pdo_options = array(
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
    );

    try {
        $db = new PDO("mysql:host={$host};port={$port};"
            . "dbname={$dbname};charset=utf8",
            $user, $password, $pdo_options);
    } catch (PDOException $e) {
        die('Database connection error: ' . $e->getMessage());
    }
        
    function message($msg, $category = 'success')
    {
        $_SESSION['message'] = array('category' => $category,
            'message' => $msg);
    }
    
    function show_messages()
    {
        if (isset($_SESSION['message' ]))
        {
            echo '<div style="margin-bottom:1em;" class="ui-bar ui-bar-'
                . ($_SESSION['message']['category'] === 'error' ? 'e' : 'b')
                . '">',
                '<h3 style="display:inline-block; width:92%">',
                $_SESSION['message']['message'],
                '</h3>',
                '<div style="display:inline-block; text-align:right; width:8%">',
                '<a id="dismiss-message" href="#" data-role="button" data-icon="delete" data-iconpos="notext" data-inline="true">Fjern</a></div>',
                '</div>';
            unset($_SESSION['message']);
        }
    }
    
    function redirect($location)
    {
        header("Location: {$location}", true, 303);
        die();
    }
    
    function get_department(PDO $db, $id)
    {
        if (!is_numeric($id) || $id < 1)
        {
            return null;
        }
        
        $sql = 'SELECT * FROM departments WHERE id = ?';
        $args = array($id);
        $stmt = $db->prepare($sql);
        $stmt->execute($args);
        if ($stmt->rowCount() === 0)
        {
            return null;
        }
        
        return $stmt->fetch();
    }
    
    function get_departments(PDO $db)
    {
        $sql = 'SELECT id, department_name FROM departments';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() === 0)
        {
            return null;
        }
        
        return $stmt->fetchAll();
    }
    
    function get_case(PDO $db, $id)
    {
        if (!is_numeric($id) || $id < 1)
        {
            return null;
        }
        
        $sql = 'SELECT id, department_id, created, expires,
            expires >= CURRENT_TIMESTAMP AS active, author, case_name, case_desc
            FROM cases WHERE id = ?';
        $args = array($id);
        $stmt = $db->prepare($sql);
        $stmt->execute($args);
        if ($stmt->rowCount() === 0)
        {
            return null;
        }
        
        return $stmt->fetch();
    }
    
    function get_cases(PDO $db, $department_id, $active = true)
    {
        if (!is_numeric($department_id) || $department_id < 1)
        {
            return array();
        }
        
        $cmp = $active ? '>=' : '<';
        $sql = "SELECT * FROM cases WHERE department_id = ?
            AND expires {$cmp} CURRENT_TIMESTAMP
            ORDER BY created DESC";
        $args = array($department_id);
        $stmt = $db->prepare($sql);
        $stmt->execute($args);
        return $stmt->fetchAll();
    }
    
    function get_comments(PDO $db, $case_id)
    {
        if (!is_numeric($case_id) || $case_id < 1)
        {
            return array();
        }
        
        $sql = 'SELECT * FROM comments WHERE case_id = ? ORDER BY created DESC';
        $stmt = $db->prepare($sql);
        $args = array($case_id);
        $stmt->execute($args);
        return $stmt->fetchAll();
    }
    
    function get_votes(PDO $db, $case_id)
    {
        if (!is_numeric($case_id) || $case_id < 1)
        {
            return array();
        }
        
        $sql = 'SELECT
                v.id AS vote_id, v.case_id, v.vote_type, v.vote_topic,
                v.votes_cast, vo.id AS option_id, vo.vote_option, vo.vote_count
            FROM votes AS v
            INNER JOIN vote_options AS vo ON vo.vote_id = v.id
            WHERE v.case_id = ?';
        $stmt = $db->prepare($sql);
        $args = array($case_id);
        $stmt->execute($args);
        return $stmt->fetchAll();
    }
    
    function str_escape($s)
    {
        return htmlentities($s, ENT_QUOTES, 'UTF-8');
    }
    
    function trim_value(&$value)
    {
        $value = trim($value);
    }

    function str_valid($str, $options)
    {
        $len = mb_strlen($str);
        if ((isset($options['max']) && $len > $options['max'])
            || (isset($options['min']) && $len < $options['min']))
        {
            return false;
        }
        return true;
    }
