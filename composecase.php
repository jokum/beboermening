<?php
    require_once 'connection.php';
    
    if (!isset($_GET['department']))
    {
        message('Afdeling ikke specificeret.', 'error');
        redirect('/index');
    }
    
    $department = get_department($db, $_GET['department']);
    
    if ($department === null)
    {
        message("Ugyldigt afdelingsnummer: {$_GET['department']}.", 'error');
        redirect('/index');
    }
    
    $_SESSION['department'] = array('id' => $department['id'],
        'name' => $department['department_name']);

    $_SESSION['time'] = time();

    if (isset($_SESSION['case']))
    {
        unset($_SESSION['case']);
    }
    
    require_once 'header.php';
?>
<script type="text/javascript">
    window.validator.rules = [
        {name: "author", fn: [text],
            options: {min_length: 3, max_length: 30}},
        {name: "case_name", fn: [text],
            options: {min_length: 10, max_length: 100}},
        {name: "expires", fn: [date],
            options: {pattern: /\d{4}[- \/]\d{2}[- \/]\d{2}/}},
        {name: "case_desc", fn: [text],
            options: {min_length: 20, max_length: 1000}},
        {name: "vote-topic", fn: [text, require_if],
            options: {min_length: 10, max_length: 30,
                dependency: {fn: is_selected, target: "voting-none"}}},
        {name: "vote-options", fn: [text, require_if],
            options: {min_length: 2, dependency: {
                fn: is_not_selected, target: "voting-config"}}}
    ];

    jQuery(document).ready(function() {
        var $voteTopic = jQuery('#voting-topic').hide(),
            $voteConfig = jQuery('.vote-config').hide();
        jQuery("input[name='voting']").change(function() {
            if (this.id === "voting-none") {
                $voteTopic.hide();
                $voteConfig.hide();
            } else if (this.id === "voting-config") {
                $voteTopic.show();
                $voteConfig.show();
            } else {
                $voteTopic.show();
                $voteConfig.hide();
            }
        });

        jQuery("#submit").click(window.validator.validate);
    });
</script>
    <form action="/savecase" method="post" data-ajax="false">
        <ul data-role="listview" data-inset="true" data-filter="false">
            <li data-role="list-divider">Afdeling <?php
                echo $department['id'], ': ',
                    str_escape($department['department_name']); ?></li>
            <li data-role="fieldcontain">
                <label for="author">Dit navn (3-30 tegn):</label>
                <input name="author" id="author" value="" data-clear-btn="true"
                    required="required" pattern=".{3,30}" type="text">
                <div class="input-error">Angiv venligst dit navn.</div>
            </li>
            <li data-role="fieldcontain">
                <label for="case_name">Sagsnavn (10-100 tegn):</label>
                <input name="case_name" id="case_name" value=""
                    data-clear-btn="true" required="required"
                    pattern=".{10,100}" type="text">
                <div class="input-error">Angiv sagens navn. Dette vises i oversigten.</div>
            </li>
            <li data-role="fieldcontain">
                <label for="expires">Udløber:</label>
                <input name="expires" id="expires" value="" required="required"
                    data-clear-btn="true" type="date" data-role="datebox"
                    data-options="{&quot;mode&quot;: &quot;calbox&quot;, &quot;calShowWeek&quot;: true,
                        &quot;overrideCalStartDay&quot;: 1, &quot;calOnlyMonth&quot;: true,
                        &quot;overrideDateFormat&quot;: &quot;%Y-%m-%d&quot;,
                        &quot;afterToday&quot;: true}"/>
                <div class="input-error">Angiv en udløbsdato. Sagen lukkes for kommentering og stemmeafgivelse kl. 23:59 på denne dato.</div>
            </li>
            <li data-role="fieldcontain">
                <label for="case_desc">Beskrivelse (20-1000 tegn):</label>
                <textarea required="required" cols="40" rows="8" name="case_desc"
                    id="case_desc"></textarea>
                <div class="input-error">Beskriv sagen. Gør teksten så kort og læselig som mulig for at forbedre sandsynligheden for respons.</div>
            </li>
            <li data-role="list-divider">Afstemning</li>
            <li data-role="fieldcontain">
                <label for="voting-none"
                    class="ui-input-text">Afstemning</label>
                <div class="ui-input-text">
                    <fieldset data-role="controlgroup" data-type="horizontal"
                        data-mini="true">
                        <label for="voting-none">Ingen</label>
                        <input type="radio" name="voting" value="0"
                            id="voting-none" checked="checked"/>
                        <label for="voting-yn">Ja/Nej</label>
                        <input type="radio" name="voting" value="1"
                            id="voting-yn"/>
                        <label for="voting-an">For/Imod</label>
                        <input type="radio" name="voting" value="2"
                            id="voting-an"/>
                        <label for="voting-config">Konfigurer</label>
                        <input type="radio" name="voting" value="3"
                            id="voting-config"/>
                    </fieldset>
                </div>
            </li>
            <li data-role="fieldcontain" id="voting-topic">
                <label for="vote-topic">Spørgsmål (10-30 tegn):</label>
                <input name="vote-topic" id="vote-topic" value=""
                    data-clear-btn="true" type="text">
                <div class="input-error">Angiv et kort spørgsmål. Dette kan muligvis være det samme som sagens navn.</div>
            </li>
            <li data-role="fieldcontain" class="vote-config">
                <label for="vote-multiple">Tillad flervalg</label>
                <input name="vote-multiple" id="vote-multiple" value="1"
                    type="checkbox"/>
            </li>
            <li data-role="fieldcontain" class="vote-config">
                <label for="vote-options">Valgmuligheder:</label>
                <textarea cols="40" rows="8" name="vote-options"
                    id="vote-options" placeholder="Adskil muligheder med en ny linie."></textarea>
                <div class="input-error">Angiv en eller flere valgmuligheder adskilt af linieskift.</div>
            </li>
            <li>
                <fieldset class="ui-grid-a">
                    <div class="ui-block-a">
                        <a href="/cases/<?php echo $department['id']; ?>"
                            data-role="button" data-rel="back">Annullér</a>
                        </div>
                    <div class="ui-block-b"><input type="submit" id="submit"
                        value="Gem"/></div>
                </fieldset>
            </li>
        </ul>
    </form>
    
<?php require_once 'footer.php';
