<?php
    require_once 'connection.php';
    
    if (!isset($_GET['case']))
    {
        message('Sagsnummer ikke specificeret.', 'error');
        if (isset($_SESSION['department']) && is_array($_SESSION['department']))
        {
            redirect("/cases/{$_SESSION['department']['id']}");
        }
        redirect('/index');
    }
    
    $case = get_case($db, $_GET['case']);
    if ($case === null)
    {
        message("Ugyldigt sagsnummer: {$_GET['case']}.", 'error');
        redirect('/index');
    }
    
    if (!$case['active'])
    {
        message('Sag udløbet.', 'error');
        redirect("/case/{$case['id']}");
    }

    $_SESSION['case'] = array('id' => $case['id'],
        'name' => $case['case_name']);

    $_SESSION['time'] = time();

    require_once 'header.php';
?>
<script type="text/javascript">
    window.validator.rules = [
        {name: "content", fn: [text],
            options: {min_length: 10, max_length: 300}}
    ];

    jQuery(document).ready(function() {
        jQuery("#submit").click(window.validator.validate);
    });
</script>
    <form action="/savecomment" method="post" data-ajax="false">
        <ul data-role="listview" data-inset="true" data-filter="false">
            <li data-role="list-divider">#<?php echo $case['id'], ': ',
                $case['case_name']; ?></li>
            <li data-role="fieldcontain">
                <label for="author">Dit navn (max 30 tegn):</label>
                <input name="author" id="author" value="" data-clear-btn="true"
                    pattern=".{0,30}" type="text">
            </li>
            <li data-role="fieldcontain">
                <label for="content">Kommentar (10-300 tegn):</label>
                <textarea required="required" cols="40" rows="8" name="content"
                    id="content"></textarea>
                <div class="input-error">Skriv en kommentar.</div>
            </li>
            <li>
                <fieldset class="ui-grid-a">
                    <div class="ui-block-a">
                        <a href="/case/<?php echo $case['id']; ?>"
                            data-role="button" data-rel="back">Annullér</a>
                        </div>
                    <div class="ui-block-b"><input type="submit" id="submit"
                        value="Gem"/></div>
                </fieldset>
            </li>
        </ul>
    </form>
    
<?php require_once 'footer.php';
