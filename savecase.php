<?php
    require_once 'connection.php';
    
    $location = '/index';

    if (isset($_SESSION['department'])
        && is_array(($dep = $_SESSION['department']))
        && isset($dep['id']))
    {
        $department = get_department($db, $dep['id']);
        
        if ($department && isset($_POST))
        {
            if (!isset($_SESSION['time']) || time() - $_SESSION['time'] < 10)
            {
                message('Vent venligst 10 sekunder med at trykke gem. Dette er for at forhindre spam.', 'error');
                redirect("/composecase/{$department['id']}");
            }
            unset($_SESSION['time']);
        
            array_filter($_POST, 'trim_value');
            
            $expires = filter_var($_POST['expires'],
                FILTER_SANITIZE_STRING);
            $author = filter_var($_POST['author'],
                FILTER_SANITIZE_STRING);
            $case_name = filter_var($_POST['case_name'],
                FILTER_SANITIZE_STRING);
            $case_desc = filter_var($_POST['case_desc'],
                FILTER_SANITIZE_STRING,
                array('flags' => !FILTER_FLAG_STRIP_LOW));

            $vote_type = (int) $_POST['voting'];
            $vote_topic = filter_var($_POST['vote-topic'],
                FILTER_SANITIZE_STRING);
            $vote_options = explode("\r\n",
                filter_var($_POST['vote-options'],
                    FILTER_SANITIZE_STRING,
                    array('flags' => !FILTER_FLAG_STRIP_LOW)));

            if (!str_valid($author, array('min' => 3, 'max' => 30))
                || !str_valid($case_name, array('min' => 10, 'max' => 100))
                || (empty($expires)
                    || !preg_match('/^\d{4}[- \/]\d{2}[- \/]\d{2}$/', $expires))
                || !str_valid($case_desc, array('min' => 20, 'max' => 1000))
                || ($vote_type > 0 && $vote_type < 4 && !str_valid($vote_topic,
                    array('min' => 10, 'max' => 30)))
                || ($vote_type === 3 && empty($_POST['vote-options'])))
            {
                message('Visse krævede værdier mangler eller opfylder ikke de specificerede kriterier.', 'error');
                redirect("/composecase/{$department['id']}");
            }

            $db->beginTransaction();
            try
            {
                $expires .= ' 23:59:59';
                $sql = 'INSERT INTO cases (department_id, created, expires,
                        author, case_name, case_desc)
                    VALUES(?, NOW(), ?, ?, ?, ?)';
                $args = array($department['id'], $expires,
                    $author, $case_name, $case_desc);
                    
                $stmt = $db->prepare($sql);
                $stmt->execute($args);
                if (!$stmt->rowCount())
                {
                    throw new PDOException('Sag ikke oprettet.');
                }

                $case_id = $db->lastInsertId();

                if ($vote_type)
                {
                    if (isset($_POST['vote-multiple'])
                        && $_POST['vote-multiple']) {
                        $multi_vote = 'check';
                    } else {
                        $multi_vote = 'radio';
                    }

                    $sql = "INSERT INTO votes
                        (case_id, votes_cast, vote_type, vote_topic)
                        VALUES(?, 0, ?, ?)";
                    $stmt = $db->prepare($sql);
                    $stmt->execute(array($case_id, $multi_vote, $vote_topic));

                    if (!$stmt->rowCount())
                    {
                        throw new PDOException('Afstemning ikke oprettet.');
                    }

                    if ($vote_type === 1) {
                        $vote_options = array('Ja', 'Nej');
                    } else if ($vote_type === 2) {
                        $vote_options = array('For', 'Imod');
                    }
                    $args = array($db->lastInsertId());
                    $sql = 'INSERT INTO
                            vote_options (vote_id, vote_count, vote_option)
                        VALUES (?, 0, ?)';
                    $stmt = $db->prepare($sql);
                    foreach ($vote_options as $opt)
                    {
                        $args[1] = $opt;
                        $stmt->execute($args);

                        if (!$stmt->rowCount())
                        {
                            throw new PDOException('Stemmemuligheder ikke oprettet.');
                        }
                    }
                }

                $db->commit();
                $location = "/case/{$case_id}";
                message('Sag oprettet. Overvej at generere <a href="'
                    . $location . '/print" title="Generér udskriveligt'
                    . ' opslag.">et opslag til ophængning</a>.');
            }
            catch (PDOException $e)
            {
                $db->rollback();
                message('Fejl under oprettelse af sag.', 'error');
            }
        }
        else
        {
            message("Ugyldigt afdelingsnummer: {$dep['id']}.", 'error');
        }
    }
    else
    {
        message('Afdeling ikke specificeret.', 'error');
    }
    
    redirect($location);
