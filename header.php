<?php require_once 'connection.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Beboermening</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<meta property="og:site_name" content="Beboermening"/>
	<meta property="og:locale" content="da_DK"/>
	<meta property="og:url" content="<?php
		echo "http://www.{$_SERVER['SERVER_NAME']}{$_SERVER['REQUEST_URI']}"; ?>"/>
<?php
if (!empty($_GET['case']) && ($case = get_case($db, $_GET['case']))):
?>
	<meta property="og:title" content="<?php echo $case['case_name']; ?>"/>
<?php
endif;
?>

    <link rel="stylesheet" href="/css/themes/bka.min.css" />
    <link rel="stylesheet" href="/css/jquery.mobile.structure-1.3.2.min.css" />
    <link rel="stylesheet" href="/css/style.css" />

    <link rel="stylesheet" type="text/css" href="/css/jqm-datebox-1.4.0.min.css" /> 

    <script src="/scripts/jquery-1.9.1.min.js"></script>
    <script src="/scripts/jquery.mobile-1.3.2.min.js"></script>
    <script type="text/javascript" src="/scripts/jqm-datebox-1.4.0.comp.calbox.min.js"></script>
    <script type="text/javascript" src="/scripts/jquery.mobile.datebox.i18n.da.utf8.js"></script>
    <script type="text/javascript" src="/scripts/validation.js"></script>
    <script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-45899009-1', 'beboermening.dk');
        ga('send', 'pageview');
        jQuery(document).ready(function() {
            jQuery(document).on("click", "#dismiss-message", function(event) {
                event.preventDefault();
                jQuery(this).closest(".ui-bar").hide();
            });
        });
    </script>
</head>
<body>
<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/da_DK/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
    <div data-role="page">

        <div data-role="header" id="header">
            <a href="#actions" data-icon="bars">Handlinger</a>
            <a href="/index" data-icon="home">Forside</a>
            <h1><?php echo isset($_SESSION['department'])
                ? ("Afdeling {$_SESSION['department']['id']}: "
                    . $_SESSION['department']['name'])
                : 'Beboermening'; ?></h1>
        </div><!-- /header -->

        <div data-role="panel" id="actions" data-position="left"
            data-display="overlay">
            <ul data-role="listview" data-icon="false">
                <li data-role="list-divider" >Handlinger</li>
                <?php if (isset($_SESSION['case'])): ?>
                <li data-icon="grid"><a data-ajax="false"
                    href="/case/<?php echo $_SESSION['case']['id'];
                    ?>/print">Generér Opslag (PDF)</a></li>
                <?php if ($_SESSION['case']['active']): ?>
                <li data-icon="edit"><a href="/composecomment/<?php
                    echo $_SESSION['case']['id']; ?>">Skriv Kommentar</a></li>
                <?php
                endif;
                endif;
                if (isset($_SESSION['department'])):
                ?>
                <li data-icon="edit"><a href="/composecase/<?php
                    echo $_SESSION['department']['id']; ?>">Opret Sag</a></li>
                <?php endif; ?>
                <a href="#header" data-rel="close" data-role="button"
                    data-mini="true">Luk</a>
            </ul>
        </div>
        
        <div data-role="content">
<?php show_messages();
