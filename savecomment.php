<?php
    require_once 'connection.php';
    
    $location = '/index';
    
    if (isset($_SESSION['case']) && is_array(($case = $_SESSION['case']))
        && isset($case['id']) && $case['id'] > 0)
    {
        if (!isset($_SESSION['time']) || time() - $_SESSION['time'] < 5)
        {
            message('Vent venligst fem sekunder med at trykke gem. Dette er for at forhindre spam.', 'error');
            redirect("/composecomment/{$case['id']}");
        }
        unset($_SESSION['time']);

        if (!isset($_POST))
        {
            message('Forespørgsel indeholder intet data.', 'error');
            redirect("/composecomment/{$case['id']}");
        }

        array_filter($_POST, 'trim_value');
        $author = filter_var($_POST['author'],
            FILTER_SANITIZE_STRING);
        $content = filter_var($_POST['content'],
            FILTER_SANITIZE_STRING,
            array('flags' => !FILTER_FLAG_STRIP_LOW));

        if (!str_valid($author, array('max' => 30))
            || !str_valid($content, array('min' => 10, 'max' => 300)))
        {
            message('Visse krævede værdier mangler eller opfylder ikke de specificerede kriterier.', 'error');
            redirect("/composecomment/{$case['id']}");
        }

        $case = get_case($db, $case['id']);
        
        if ($case)
        {
            if (!$case['active'])
            {
                message('Sag udløbet.', 'error');
                redirect("/case/{$case['id']}");
            }

            $sql = 'INSERT INTO comments (case_id, created, author, content)
                VALUES(?, NOW(), ?, ?)';
            $args = array($case['id'], $author, $content);
            $stmt = $db->prepare($sql);
            $stmt->execute($args);
            if ($stmt->rowCount() === 1)
            {
                message('Kommentar gemt.');
                $location = "/case/{$case['id']}";
            }
            else
            {
                message('Kommentar ikke gemt.', 'error');
            }
        }
        else
        {
            message("Ugyldig sagsnummer: {$case['id']}.", 'error');
        }
    }
    else
    {
        message('Sag ikke specificeret.', 'error');
    }
    
    redirect($location);
